from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateForm, CreateCategory, CreateAccount

# Create your views here.


# This view allows us to see all instances of the receipt model
@login_required()
def see_receipt(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt": receipt}
    return render(request, "receipts/home.html", context)


# This allows a user to create a receipt
@login_required()
def create_receipt(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    else:
        form = CreateForm()

    context = {"form": form}

    return render(request, "receipts/create.html", context)


# This view allows the user to see expense caregories and the number of
#  receipts in that category. It is filtered to only show data
# for the current user
@login_required()
def category_list(request):
    cats = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "cats": cats,
    }
    return render(request, "receipts/categories.html", context)


@login_required()
def account_list(request):
    acc = Account.objects.filter(owner=request.user)
    context = {
        "acc": acc,
    }

    return render(request, "receipts/accounts.html", context)


# This view allows the logged in user to create their own category
@login_required()
def create_category(request):
    if request.method == "POST":
        form = CreateCategory(request.POST)
        if form.is_valid():
            cat = form.save(False)
            cat.owner = request.user
            cat.save()
            return redirect("category_list")
    else:
        form = CreateCategory()
    context = {"form": form}
    return render(request, "receipts/cat_create.html", context)


# This view allows the logged in user to create their own account
@login_required()
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            acc = form.save(False)
            acc.owner = request.user
            acc.save()
            return redirect("account_list")
    else:
        form = CreateAccount()
    context = {"form": form}
    return render(request, "receipts/acc_create.html", context)
