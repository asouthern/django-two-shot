from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignUpForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.


# This function allows a user to login
def user_login(request):
    # these lines will login the user if it is a POST method
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")
    # this line will display that form when it is a GET method
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


# This function allows a user to logout
def user_logout(request):
    logout(request)
    return redirect("login")


# THis function allows a user to signup
def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

        if password == password_confirmation:
            user = User.objects.create_user(
                password=password,
                username=username,
            )
            login(request, user)

            return redirect("home")
        else:
            form.add_error("password", "the passwords do not match")

    else:
        form = SignUpForm()
    context = {
        "form": form,
    }

    return render(request, "accounts/signup.html", context)
